# Yocto Poky and AutoSD

WARNING: early stage project.

This repository serves as a guide on how to build meta-autosd with Yocto Poky.

This document is using Fedora as the base system reference.

## Dependencies

You can use a predefined toolbox image by running: `toolbox create --image quay.io/lrossett/yocto-toolbox:latest yocto-dev`.

You can also use the toolbox container file as a reference to what packages need to be installed in order to build
Poky: https://gitlab.com/autosd-on-yocto/toolbox/-/blob/main/Containerfile?ref_type=heads#L3-37.

## Building

You need clone poky first:

```sh
git clone git://git.yoctoproject.org/poky
```

The following commands will assume you are inside the `poky` directory.

Now cd into poky and clone the following layers inside your `poky` dir (kirkstone branch):

* git://git.openembedded.org/meta-openembedded
* git://git.yoctoproject.org/meta-selinux
* git://git.yoctoproject.org/meta-virtualization
* https://gitlab.com/autosd-on-yocto/meta-autosd.git (use master branch)

You can now source into the openembedded development environment:

```sh
source oe-init-build-env
```

That command will change your working directory to `poky/build`.

You nown need to add the following layers into your Poky distribution (you may need to add one at a time):

```sh
bitbake-layers add-layer ../meta-openembedded/meta-networking
bitbake-layers add-layer ../meta-openembedded/meta-filesystems
bitbake-layers add-layer ../meta-openembedded/meta-oe
bitbake-layers add-layer ../meta-openembedded/meta-python
bitbake-layers add-layer ../meta-virtualization
bitbake-layers add-layer ../meta-autosd
```

You can run `bitbake-layers show-layers` or check the contents of `conf/bblayers.conf` to assert if such layers were
properly added.

Now add the following content at the end of `conf/local.conf`:

```
# Uncomment to limit threads used by bitbake. Set values according to your dev. env.
# BB_NUMBER_THREADS = "2"
# PARALLEL_MAKE = "-j 2"

DISTRO_FEATURES:append = " usrmerge systemd acl xattr pam"
DISTRO_FEATURES_BACKFILL_CONSIDERED += "sysvinit"

VIRTUAL-RUNTIME_init_manager = "systemd"
VIRTUAL-RUNTIME_initscripts = "systemd-compat-units"
```

You are now ready to build a minimal Poky image with some of the AutoSD stack by running: 

```sh
bitbake autosd
```

Run it using Yocto's qemu wraper script:

```sh
runqemu qemux86-64
```
